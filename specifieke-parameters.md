# Specifieke Parameters

Voor het Project zwemwaterkwaliteit voor de Belgische kust zijn een aantal specifieke parameters en outputs geconfigureerd in het systeem. Deze zijn hieronder opgenomen.

## Workflow

De workflow die is opgezet om van een overstort tot een waarschuwing te komen is weergegeven in onderstaand diagram.

![](/assets/workflow.png)

## Overschrijdings analyse

De overschrijdings analyse van de E.coli in I.E gebeurd enkel voor waarden gesimuleerd tussen 7u en 18u UTC. Dit komt overeen met de periode waar gezwommen mag worden plus een buffer van 1.5u.

## Email operatoren

Indien een overschrijding wordt gesimuleerd wordt een automatische email uitgestuurd naar alle operatoren \(zie vb hieronder\). Deze kunnen de simulatie dan controleren en de resultaten desgewenst publiceren.

Beste,

Er is een overschrijding van de zwemwaterkwaliteit gesimuleerd. Gelieve u te melden op [http://zwemwaterkwaliteit.imdcapps.be](http://zwemwaterkwaliteit.imdcapps.be) en deze simulatie te onderzoeken.

mvg,

Synapps zwemwaterkwaliteit

## Email overheden

Bij publicatie van een simulatie wordt een email uitgestuurd naar de relevante overheden \(zie vb hieronder\). Ook wordt de data van de externe API geupdate zodat de website kwaliteitszwemwater kan worden geupdate.

Beste,

Er worden overschrijdingen van de maximale concentratie fecale bacterien verwacht in de komende dagen. Gelieve de gepaste actie te ondernemen. Meer details omtrent de locatie en de periode zijn te vinden op www.kwaliteitzwemwater.be.

Mvg,

Synapps zwemwaterkwaliteit

## Output locaties

De locaties met waar het model resultaten genereerd en waarop een overschrijdings analyse gebeurd zijn hieronder opgenomen.

![](/assets/Output_locaties.PNG)

