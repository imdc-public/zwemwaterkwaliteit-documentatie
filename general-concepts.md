# Algemene concepten

## Inleiding

Het Synapps systeem is volledig webgebaseerd en bevindt zich in de cloud. Het systeem kan worden geraadpleegd via volgende webpagina:

[https://zwemwaterkwaliteit.imdcapps.be/](https://zwemwaterkwaliteit.imdcapps.be/)

Indien u niet bent ingelogd, wordt u doorverwezen naar een pagina waarin u uw inloggegevens kan invullen. In een volgende stap wordt u verder geleid naar het [dashboard](/dashboard.md). Het dashboard is het kloppende hart van het systeem. Via de menubalk kan u verder navigeren naar de volgende pagina's:

* _Home _\(_Dashboard_\)
* _Observations_
* _Overflow_
* _Simulations_
* _Mapviewer_

## Architectuur

Het Synapps systeem is als een modulair systeem ontworpen, wat betekent dat verschillende modules afzonderlijk draaien en geïntegreerd zijn tot 1 systeem. De modules in deze setup zijnn:

* **OFS**: Deze module is verantwoordelijk voor alles wat te maken heeft met het runnen van de simulaties.
* **OSS**: Deze module is verantwoordelijk voor alles wat te maken heeft de geobserveerde data.
* **Front-end**: Dit is wat wordt gezien door de gebruiker.



