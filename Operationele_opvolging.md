
# Operationale opvolging

## Publiceren overschrijding

Wanneer door het voorspellingssysteem een overschrijding van de concentraties fecale bacteriën wordt gedetecteerd, wordt automatisch een waarschuwingsmail naar de operatoren gestuurd.
Alvorens een operator vervolgens een eventuele overschrijding goedkeurt en publiceert, dient de operator een voorspelling te controleren waarbij hij moet nagaan of het voorspellingsresultaat logisch is en in lijn is met de verwachtingen.
In het voorspellingssysteem zijn verschillende modules beschikbaar waarin de operator de voorspellingen kan controleren zoals “Dashboard”, “Simulations”, “Mapviewer”. Zie de uitleg in de manual voor de werking van deze modules. Hieronder worden de belangrijkste aandachtspunten opgelijst:


* Controleer de status van de simulatie in het paneel “Simulations” in het “Dashboard” paneel of in het tabblad “Simulations”. Een voorspellingssimulatie dient een status “succes” te hebben alvorens goed te keuren.
* Ga naar tabblad [Simulations](/simulations.md)
    * Open de simulations detail page;
    * Controleer de tabel met overstortdata: vanaf wanneer welke overstorten actief waren;
    * Controleer de heatmap op locaties en dagen met overschrijdingen en of deze overeenkomen en logisch zijn ten opzichte van de overstortdata.
    * Indien er twijfel is over de overstorten kan nagegaan worden of het overstort event overeenkomt met een neerslagevent   , waarvan de meest recente gegevens kunnen worden geraadpleegd op https://www.waterinfo.be/kaartencatalogus .
* Ga naar [mapviewer](/mapviewer.md)
    * Controleer de tijdreeksen met concentraties aan de overstorten. Komen deze overeen met de verwachting van een overstort (bijvoorbeeld in een zeer droge periode is een overstort onwaarschijnlijk) ?
    * Controleer de tijdreeksen met concentraties en waterstanden op de meetpunten.

Indien nog een voorspellingssimulatie aan het rekenen is (zie kolom status en progress in overzichtskolom simulaties) kan worden gekozen om de resultaten van deze voorspelling af te wachten alvorens te publiceren. De operator dient deze afweging te maken.
Na goedkeuring van een voorspelling, verschijnt een popup dat een waarschuwingsemail wordt rondgestuurd naar de relevante diensten. De operator kan controleren of de mail verzonden is via de eigen mailbox.

## Mogelijke foutmeldingen

De volgende storingen kunnen optreden in het systeem:
* Het voorspellingssysteem is offline of reageert niet
    * Controleer de internetverbinding.
    * Ververs de pagina. Gebruik de toetsencombinatie “Ctrl F5”.
    * Er zijn mogelijk problemen met de externe server. Contacteer support.
* Automatische download windvelden faalt.
    * Onder het tabblad Home of Observations staat een overzicht van de meest recent gedownloade windvelden (GFSOpendap)
    * Windgegevens worden gebruikt in het voorspellingssysteem. Deze gegevens worden automatisch gedownload. Er worden telkens voorspellingen voor 8 dagen in de toekomst gedownload. Het ontbreken van windvelden geeft een probleem voor het voorspellingssysteem als de laatst beschikbare data ouder is dan 5 dagen. 
    * Contacteer support.
* Status van simulatie staat op “failed”
    * Controleer de logfiles in de “Simulations detail page »
	* In de meeste gevallen zal er een storing zijn op de server, neem contact met support.
    * Indien mogelijk, start de simulatie manueel op in de tab “Simulations”
* Simulatiemachine is offline
    * De status van elke rekencomputer is aangeduid in de rechterbovenhoek van het scherm met het symbool ![](/assets/cm2.png). De kleur geeft de beschikbaarheid aan zoals beschreven in de manual van het [dashboad](/dasboard.md)
    * Neem contact op met support indien het symbool rood is.
* Overstort
    * Indien geen overstortgegevens worden doorgestuurd door Aquafin wordt een automatische waarschuwingsmail gestuurd naar Aquafin en support.
    * Bij sterke aanwijzingen van mogelijk overstortwerking kan de operator controleren of de pompen in werking zijn getreden in tab “Overflow” en of er een voorspellingssimulatie is uitgevoerd of aan het rekenen is in tab “Simulations”. Er kan worden gekozen door de operator om manueel een overstort in te voeren waarna het voorspellingssysteem zal starten.
* De operator krijgt geen waarschuwingsmails
    * Er zijn geen overstorten geweest of doorgestuurd door Aquafin.
    * Er zijn toch overstorten geregistreerd, maar de voorspellingssimulatie leidt niet tot een overschrijding van de zwemwaterkwaliteit. Controleer de voorspellingen in de tab “Simulations” of “Mapviewer”.
    * De operator zit niet in de maillijst.
* Er start geen automatische voorspelling
    * Er is geen overstort binnen gekomen, zie [overstort](basis-parameters.md).
    * De schedule is gedeactiveerd. Onder de tab “Simulations” is standaard een schedule geactiveerd die automatisch een simulatie opstart na het binnenkomen van een overstortevent. Indien deze scheduler is gedeactiveerd wordt geen automatische simulatie opgestart. Contacteer support in dat geval om de scheduler terug aan te maken.
    
Voor andere zaken neem contact op met support.
