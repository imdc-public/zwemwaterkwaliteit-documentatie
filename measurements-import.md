# Import metingen

Meeting kunnen ingevoerd worden in het systeem via de configuratie interface. Deze interface is bereikbaar via de zwemwater portaalsite: [https://zwemwaterkwaliteit.imdcapps.be/\](https://zwemwaterkwaliteit.imdcapps.be/)

![](/assets/config_interface_ingang.png)

Vervolgens wordt onderstaande interface getoond waarbij de tool "Frontend Zwemwaterkwaliteit" dient geselecteerd te worden.

![](/assets/config_interface_zwemwater.png)

De overzichtsconfiguratie van de frontend zwemwaterkwaliteit bevat informatie over de zoomlevel en de coördinaten van het startscherm van de mapviewer. Deze kunnen hier aangepast worden. Onder de knop Observations \(zie hieronder\) kan worden doorgeklikt om de metingen op te laden.

![](/assets/config_interface_link_observations.png)

Het volgende scherm bevat een drag en drop veld waarin de nieuwe tijdsreeksdata in csv formaat kan worden opgeladen. Elke upload moet steeds een configuratie file bevatten met de naam 'sites\_config\_server.csv". Deze configuratie file bevat alle metadata van de opgeladen tijdreeks files zodat deze correct in de frontend worden weergegeven. Een voorbeeld van de config file en de tijdreeksfiles worden mee opgeleverd ([file](https://imdc.gitlab.io/zwemwaterkwaliteit-documentatie/assets/sites_config_server.csv))

![](/assets/config_interface_input_data.png)

Door op de knop save te klikken wordt een proces opgestart op de server. Dit proces zal alle csv files inlezen en opslagen in de database. de status en voorgang van dit proces kan gevolgd worden.

![](/assets/config_interface_save_data.png)

