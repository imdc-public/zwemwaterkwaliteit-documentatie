# Zwemwaterkwaliteit

Het Agentschap Zorg en Gezondheid is samen met de Vlaamse Milieumaatschappij verantwoordelijk voor het toezicht op de waterkwaliteit aan de kust en in zwem- en recreatievijvers. Om baders en waterrecreanten aan de Belgische kust gericht te kunnen waarschuwen voor bacteriologische verontreinigingen is door IMDC een operationeel voorspellings- en waarschuwingssysteem ingezet.



