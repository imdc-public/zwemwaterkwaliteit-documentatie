# Synapps

Het Synapps operationeel voorspellings- en waarschuwingssysteem voor zwemwaterkwaliteit is ontworpen om de impact van een overstort van de riolering op de zwemwaterkwaliteit in de Noordzee te monitoren en de bevoegde diensten te waarschuwen bij een slechte zwemwaterkwaliteit.



Deze handleiding beschrijft het Synapps systeem dat momenteel is geïmplementeerd voor de Belgische kust. De handleiding volgt de structuur van het web platform zodat de operator meteen de juiste sectie kan vinden waar hij/zij geïnteresseerd in is.

