# General concepts

There are different entry points to the mapviewer and the resulting page depends on how you arrive at the MapViewer:

* By clicking on the MapViewer link in the menu bar. The result will be an empty instance of the MapViewer 
* By clicking on the View Results action button in the [simulation overview table](/simulation-overview.md). The result will be a MapViewer that has that simulation loaded.
* By clicking on the View On Map button next to an [observation point](/dashboard.md). The result will be a MapViewer that has the last month of data of this simulation loaded.
* By clicking on the permalink you received from someone else. The result is a MapViewer in the same state as it was when the link was generated. See the explanation of the Share this map functionality below.

The mapviewer consists of different modules. The functional modules correspond to the different modules that have been explained in the previous parts of the manual. They are grouped on the left side of the screen. The general modules contain information and functionality that is shared between the modules. They are grouped on the right side of the screen.

## Graphs

When selecting an observation point or a model output point, the MapViewer will transition to a graph state. This means that the map will shrink and a graph will appear at the bottom of the screen. All graphs in the system are interactive which means they allow for zooming, panning and retrieving all values. This section will explain some general concepts related to the graph interaction. Some specific aspects for observation points and simulation output is discussed in the respective sections.

### View, analyze and export

The values on the graph can be read from the mouse tooltip when hovering over the series. When a site contains multiple variables the first variable will be shown on the primary y-axis \(left\) and subsequent variables will be shown on secondary y-axes \(right\). The graph can be exported by clicking on the ![](/assets/ExportGraph.png) icon. Possible export formats are:

* PNG
* JPEG
* PDF
* SVG

The zoom control in the upper left corner has following options:

* ![](/assets/HorZoom.png): Select horizontal zoom option \(default\)
* ![](/assets/VerZoom.png): Select vertical zoom option
* ![](/assets/PanZoom.png): Select pan

Zooming can be done by dragging the mouse pointer from left to right or from bottom to top, depending on the selected zoom option. Click the button `Reset zoom` to reset the view to the original zoom level. Panning can be done by holding the left mouse button and dragging the graph around. When the pan option is not selected it is still possible to pan by clicking shift before clicking on the graph.

![](/assets/GraphZoomed.png)

### Combine two sites

Two observation sites or model output points can be visualized on the same graph. As shown below the dash style of the sites will differ and the different sites will be indicated by the icons ![](/assets/Site1.png) and ![](/assets/Site2.png) in the map. The visibility of all series on the graph linked to a certain sites can be toggled in group by clicking on the serie in the Sites panel left to the graph \(see example below\). A site can be deleted from the graph again by clicking on the red cross next to the site.

![](/assets/mapviewer_ts.png)

![](/assets/MultipleSites2.png)

### Combine simulations with measurements

To combine simulation results with measurement data it is necessary to [toggle the visibility of the observation data](/observation.md) and load the [simulation output points](/mapsimulation.md) into the map. Once both are visible you can click on a location in the map that has both observation data and a model output point to show both on the graph as illustrated below. The measurements and simulation output will receive a different dash style. The visibility of the series can be toggled and the series can be deleted on a similar manner as explained above.



## Share this map

The `Share this map`functionality can be found on the right hand side of the menu bar and allows an operator to share a map in a specific state with other users of the system \(operators with login\). The operator retrieves a unique url that can be used to share with other users. Upon clicking on the url that other person immediately sees the simulation, map layer\(s\), graph\(s\) and timestamp that the operator had in his map when he requested the url. Thus allowing an operator to quickly share interesting information.

![](/assets/share2.png)

