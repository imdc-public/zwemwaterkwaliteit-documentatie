# Summary

* [Wat is Zwemwaterkwaliteit?](README.md)
* [Introductie](introduction.md)
* [Algemene concepten](general-concepts.md)
* [Dashboard](dashboard.md)
* [Observations](observations.md)
* [Simulations](simulations.md)
  * [Algemene simulatie concepten](simulations/sdf.md)
  * [Simulation overview](simulations/simulation-overview.md)
  * [Simulation details page](simulations/simulation-details-page.md)
  * [Start een simulatie](simulations/start-a-simulation.md)
* [Overstort](basis-parameters.md)
* [MapViewer](mapviewer.md)
* [Import metingen](measurements-import.md)
* [Specifieke parameters](specifieke-parameters.md)
* [Operationele opvolging](Operationele_opvolging.md)

