## Overstort

De pagina Overflow verzamelt alle overstortgegevens, zowel de manueel ingevoerde als de automatisch geregistreerde overstorten.

![](/assets/Overstort_overzicht.PNG)

## 

Per locatie worden door de administrator een aantal default waarden geconfigureerd:

•     Droogwaterafvoer volume DWA \(m³\)

•    Concentratie IE \(KVE/100ml\)

•    Concentratie EC \(KVE/100ml\)

•    Tijd nodig voor het terug vullen van de DWA na een event, filltime \(uren\)

De volgende parameters worden per event ingegeven \(automatisch of manueel\):

•    Overstortlocatie

•    Start en stoptijden overstort

•    Debiet \(m³/s\)

•    Type input \(manuele invoer of automatisch\)

Bij het invoeren van de data zijn volgende automatische checks voorzien:

•    Controle of startdatum voor einddatum ligt

•    Controle of events op 1 overstortlocatie niet overlappen

De operator kan een nieuw overstort event toevoegen met de knop \(Add Overflow\) en data van een bestaand overstort event aanpassen \(blauwe knop achter elke event, ![](/assets/Overstort_aanpassen_knop.PNG)\).

## Tijdreeksen

Bij de opstart van een nieuwe voorspelling of bij het visualiseren van de data in de overstort database worden door het Synapps voorspellingssyteem tijdreeksen aangemaakt. Deze tijdreeks kan in de tool geconroleerd worden door op de knop ![](/assets/Overstort_ts_knop.PNG) te klikken, zichtbaar na elke overstortlijn.Op basis van de geregistreerde overstort events wordt per overstortlocatie een tijdreeks aangemaakt bestaande uit het debiet aan water dat geloosd wordt en de concentratie aan fecale bacteriën IE en IC \(KVE/100ml\). Bij het aanmaken van de tijdreeksen worden de volgende beslissingsregels toegepast:

•    Bij de start van een event wordt via een constant debiet zoetwater met de vastgelegde \(default\) concentratie bacteriën geloosd. De duurtijd van deze lozing is gelijk aan het startvolume \(normaal gelijk aan het DWA volume\) gedeeld door het debiet.

•    Indien het event langer duurt dan de tijd om met het debiet het vuilvrachtvolume te lozen wordt er na deze tijdsspanne een zoetwater debiet met een nul concentratie aan bacteriën geloosd.

•    Indien een event snel op een voorgaand event volgt is de verwachting dat ter hoogte van de overstort nog niet de volledige capaciteit aan vuilvracht \(DWA volume\) zal opgebouwd zijn. De duurtijd van de storting met vastgelegde concentratie bacteriën zal daarom verkort worden. Het startvolume vuilvracht van dit nieuwe event wordt bepaald door het eindvolume van het voorgaande event vermeerderd met de tijd tussen de twee events maal het DWA volume gedeeld door de filltime. Hierbij wordt de DWA volume evenwel als maximum aangehouden.

![](/assets/Overstorft_ts.PNG)

In geval van het Caemerlinckxgeleed worden de bovenstaande regels toegepast om een tijdreeks van de overstort aan de Blauwkasteelstraat te bepalen welke niet rechtstreeks in de haven van Oostende loost, maar in het aftwateringsnetwerk van de stad. De vertaling van de lozing door het overstort naar de lozing via het Caemerlinckxgeleed in de haven van Oostende gebeurt aan de hand van het boxmodel . Dit boxmodel is een vereenvoudigde schematisatie van de processen die de afvoer bepalen \(zoals afvoer krekengebied, gravitaire lozing met terugslagklep, werking van noodpompen\).

## Locaties

Volgende overstort locaties met bijhorende parameters zijn geimplementeerd in het systeem.

# ![](/assets/Overstort locaties.PNG)

# Automatische import van overstorten

Aquafin beheert een deel van de overstort pompen die als input dienen voor het systeem. Een IOT toepassing is opgezet waarbij een API PUSH call wordt uitgevoerd telkens een pomp in werking treedt of uitschakelt . Per overstort locatie zijn verschillende pompen actief. Naargelang de hoeveelheid pompen dat in werking treedt, zal het debiet van het overstort verschillen. Volgende pompen zijn geïmplementeerd. De pompen in het Leopoldspark en het Montgomerydok worden niet beheerd door Aquafin. Daarom wordt dezelfde input aangenomen als in respectievelijk Waterkasteelstraat en Opex.

Caemerlinckxgeleed

* blauwkasteelstraat\_vijzel1swa
* blauwkasteelstraat\_vijzel2swa
* blauwkasteelstraat\_vijzel3swa

Opex

* opex\_swa
* opex\_pomp1opex
* opex\_pomp2opex
* opex\_pomp3opex
* opex\_pomp1vismijn
* opex\_pomp2vismijn

Waterkasteelstraat

* waterkasteel\_pomp1swaa
* waterkasteel\_pomp1swab
* waterkasteel\_pomp2swaa
* waterkasteel\_pomp4swaa
* waterkasteel\_pomp3swaa

Kattesas

* kattesas\_pomp1swa
* kattesas\_pomp2swa
* kattesas\_pomp3swa

Volgende intelligentie is geimplementeerd in het systeem

* binnenkomende start en eind van een pomp --&gt; event wordt aangemaakt voor de locatie waar de pomp in geconfigureerd
* binnenkomende start van een pomp --&gt; event met open eind tijdstip wordt aangemaakt
* binnenkomende stop van een pomp --&gt; het event met open eind tijdstip wordt afgesloten
* binnenkomende start van een pomp en pomp staat al geregistreerd als aan
  * als nieuwe starttijdstip verder dan 12u in de toekomst ligt tov voorgaande starttijdstip wordt het eerste event afgesloten met een duur van 1uur en wordt een nieuw event aangemaakt. Er wordt daarbij verondersteld dat pompen niet 12u continu zullen draaien in realiteit. Het afsluiten van het eerste event na 1 uur zorgt ervoor dat zeker het daaropvolgende event dat binnenkomt een volledige vuilvracht wordt geloosd omdat deze terug opnieuw kon opbouwen in het rioolstelsel.
  * als nieuwe starttijdstip minder is verder dan 12u in de toekomst ligt wordt een foutmelding gegenereerd dat de pomp al in werking is. Er worden geen acties genomen in de database met overstorten.
* binnenkomende start van een pomp op een locatie waar reeds één of meerdere pomp\(en\) in werking is --&gt; nieuw event wordt aangemaakt met als debiet de som van de  pompen

* binnenkomende stop van een pomp op een locatie waar meerdere pompen in werking zijn --&gt; Lopende event wordt afgesloten met als debiet de som van de toen werkende pompen en nieuw event wordt aangemaakt met als debiet de som van nog werkende pompen. 

Daarboven zijn er verschillende checks geïmplementeerd die nagaan of de pomp geconfigureerd is, of het formaat corrects is, of de tijdstippen logisch zijn etc.

