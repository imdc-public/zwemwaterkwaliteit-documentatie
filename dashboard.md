# Dashboard

Het dashboard is de plaats waar de gebruiker meteen een overzicht krijgt van het hele systeem.

Het is opgebouwd uit twee componenten met elk een specifieke functie: De observatie en simulatie component. Dit hoofdstuk gaat verder in op deze twee componenten.

## Observations

De component Observations geeft een overzicht van alle binnenkomende data. Deze gegevens kunnen worden onderverdeeld in:

* Randvoorwaarden nodig voor de modellering \(momenteel "GFSOpendap" genoemd\)
* Data van observatiepunten \(momenteel "measurements" genoemd\)

![](/assets/observations.png)

Het tabblad "GFSOpendap" geeft een overzicht van de GFS data die is gedownload in het systeem. Normaal gezien wordt elke 6 uur een nieuwe set aan data gedownload. Er verschijnt een foutmelding indien er iets mis is met de gegevens of met het downloadscript. Gelieve de helpdesk te contacteren in een dergelijk geval.

![](/assets/observations_measurements.png)

Het tabblad "measurements" geeft een overzicht van de binnenkomende data. Deze data wordt geïmporteerd door de operator. De tabel geeft een overzicht van alle stations en variabelen, alsook de datums van de laatste tijdstap waarom date van een variabele beschikbaar is. Indien deze datum later is dan verwacht \(op basis van de geconfigureerde frequentie\), zal een melding verschijnen, zoals weergeven in bovenstaande figuur. Deze melding geeft met andere woorden een potentieel probleem aan.

Alle tabellen zijn dynamisch wat impliceert dat gebruikers zelf het aantal, de compositie of volgorde van de getoonde reeksen kan aanpassen. Indien u klikt op '_View on Map_' wordt u verder geleid naar de [MapViewer ](/mapviewer.md)waar de data geografisch kan worden geïnspecteerd.

## Simulations

De component "Simulations" bevat informatie over de meest recente simulatie en de status van de rekencomputers.

De status van elke rekencomputer is aangeduid op de rechterbovenhoek met het symbool ![](/assets/cm2.png). De kleur geeft de beschikbaarheid aan:

* Groen: Beschikbaar om een simulatie te starten.
* Oranje: Momenteel aan het simuleren. Nieuwe simulaties worden op de wachtlijst geplaatst.
* Rood: De rekenmachine blijkt offline te zijn of er is een additioneel probleem. 

Indien u boven de rekencomputers blijft hangen, wordt bijkomende informatie getoond in de popup vensterp. Op dit moment is slechts één rekencomputer beschikbaar.

De tabel zelf toont enkele gegevens over de laatste simulaties. De betekenis van de verschillende kolommen en actieknoppen is beschreven in de sectie [Simulations](/simulations.md).

![](/assets/dashboard_simulations.png)

## 



