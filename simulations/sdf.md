# Algemene concepten

## Simulatie

Een simulatie kan in deze context worden gezien als het runnen van een opeenvolging van de modellen, ook wel een modellentrein genoemd, met één bepaalde configuratie. Een simulatie is een combinatie van numerieke modellen \(zoals Telemac\) en een collectie van functies geschreven in een programmeertaal \(zoals _nesting scripts_\). Een simulatie kan worden opgestart door de operator \(handmatige simulatie\) of op basis van een vooropgesteld programma \(automatische simulatie\). De verschillende onderdelen uitgevoerd tijdens de simulatie zijn beschreven in de configuratie van de modellentrein.

## Modeltrein

Een modeltrein linkt de verschillende simulatie delen en processen in een logische volgorde. Eens de modeltrein is geconfigureerd kan de gebruiker deze selecteren in het platform zodat alle simulatie- en procesdelen gedefinieerd in de trein worden uitgevoerd.

## Simulatie onderdeel

Een typisch simulatie onderdeel bestaat uit de configuratie van een numerieke simulatie met pre- en postprocessing scripts. Wanneer een simulatie onderdeel is opgestart, zal een numerieke simulatie starten op een rekencomputer. Een simulatie onderdeel is in dit geval een telemac simulatie waard de waterbeweging en de verschrijding vand e vervuiling wordt berekend. 

## Proces onderdeel

Processen kunnen gedefinieerd worden als scripts die \(in het algemeen\) exclusief zijn geschreven voor het platform en worden uitgevoerd tijdens een modellentrein samen met de simulaties. Nestings processen zijn een typisch voorbeeld van een proces deel. Een nesting proces is een functie dat een link vormt tussen verschillende simulatie delen. Zo is er bijvoorbeeld in het platform een nestings proces gedefinieerd dat de output van een globaal hydrodynamisch model converteert naar de input voor een gedetailleerd 3D hydrodynamisch en fecale bacterien transport model.

## Automatische simulatie \(schedule\)

Een automatische simulatie start  een voorafgaand gedefinieerd tijdsinterval \(frequentie\) of op een specifiek triggerpunt. De configuratie bepaalt de toegepaste modellentrein, de simulatie periode, het model scenario, etc. Een specifiek triggerpunt kan bijvoorbeeld de automatische detectie van een overstort zijn.

## Voorspellings periode \(Forecast period\)

Een veld dat aangeeft tot hoeveel dagen na de RO een simulatie moet lopen \(enkel voor automatische simulatie\)

## Hindcast periode

Een veld dat aangeeft hoeveel dagen voor de RO een simulatie moet starten. Deze periode wordt meestal gebruikt als inloop periode.

## Randvoorwaarden

Sommige simulatie onderdelen hebben bepaalde randvoorwaarden nodig. Dit kunnen bijvoorbeeld wind en drukvelden zijn. Deze randvoorwaarden worden automatisch gedownload. Aan het begin van een simulatie wordt nagegaan of ze effectief beschikbaar zijn voor de gehele simulatie periode. Als er geen data beschikbaar is geeft dit een foutmelding.

## Sites

Sites zijn puntlocaties waarvoor output wordt gegenereerd door de modellen. Een site is steeds gekoppeld met een simulatie onderdeel en een set van specifieke output variabelen \(water levels, concentraties,...\).

## Overschrijdingslimieten \(Tresholds\)

Overschrijdingslimieten kunnen gedefinieerd worden voor een variabele op een bepaalde site en gelinkt met een simulatie onderdeel. Er kunnen maximaal 3 limieten worden vastgelegd per site en variabele \( in deze configuratie zijn er 2 opgelegd, de limiet aanvaardbaar en de limiet slecht\). Deze zullen met een andere kleur worden weergegeven. De overschrijdingsanalyse wordt uitgevoerd op een  dagelijkse basis tussen 7u UTC en 18u UTC. De hoogste overschrijding wordt weergegeven in de heatmap.

![](/assets/thresholds.png)

## 

## Rekencomputer

Een simulatie onderdeel start een numerieke simulatie op een rekencomputer. Elke geconfigureerde rekenmachine kan 1 simulatie tegelijkertijd uitrekenen. Als er meerdere simulaties worden opgestart op dezelfde rekencomputer zullen deze in een wachtrij komen te staan.

