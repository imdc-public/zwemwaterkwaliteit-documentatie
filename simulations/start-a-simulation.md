# Start een simulatie

Simulaties kunnen worden opgestart door te drukken op de `Create Simulation` knop . Een pop-up zal vervolgens verschijnen met verscheidene invoervelden \(zie tabel en figuren hieronder\). Meer informatie over elke parameters is te vinden bij de [Algemene simulatie concepten](/simulations/sdf.md). Na het invullen van de juiste paramaters, klikt u op `Save` om de simulatie op te slaan in het systeem. Eens de simulatie is opgeslagen, zal het systeem alle acties beschreven in de modeltrein configuratie  opstarten. De status van de run zal op 'running' worden gezet. Indien de geselecteerde simulatiemachine \(tijdelijk\) niet beschikbaar is, zal het systeem de simulatie in een wachtrij plaatsen waarna deze wordt uitgevoerd wanneer de simulatiemachine wel beschikbaar is.

| Naam | Beschrijving |
| :--- | :--- |
| Type of Simulation | Het type van de simulatie: manueel/automatisch. Dit veld is onveranderlijk en kan niet worden aangepast door de operator. |
| Name | De naam van de simulatie |
| Modeltrain | De modellentrein van de simulatie |
| Start date | Startdatum van de simulatie |
| End date | Einddatum van de simulatie |
| Test Simulation | Alleen te gebruiken voor test of ontwikkeling doeleinden. De operator dient dit niet te gebruiken. |
| Use Hotstart | Niet geïmplementeerd voor dit project |
| Create Hotstart | Niet geïmplementeerd voor dit project |
| Simulation machine | Rekencomputer waarop de simulatie wordt uitgevoerd |
| Description | Beschrijving van de run. Dit kan nog steeds worden aangepast in een latere fase. |

![](/assets/start_sim.png)

![](/assets/CreateSimulation_form.png)

