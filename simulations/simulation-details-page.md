# Simulatie details pagina

De simulatie details bevatten verschillende sectie die samen een omvattend overzicht geven van de simulatie. De verschillende secties zijn aangegeven in de volgende figuur en worden in wat volgt één voor één verduidelijkt.

![](/assets/simulatie_overzicht.png)

### 1. Algemene informatie

Deze sectie herhaalt bepaalde metadata van de simulatie. De meeste parameters zijn behandeld in de [Simulation overview](/simulation-overview.md) pagina. De paramater `machine` geeft aan op welke rekencomputer de simulatie is gerund of aan het runnen is.

### 2. Water kwaliteit informatie

Deze sectie geeft een samenvatting van de informatie die gelinkt is aan een potentiële overschrijding van de waterkwaliteit. De resultaten zijn samengevat in een figuur. De x-as van de figuur bevat verschillende locaties waarvoor overschrijdingslimieten zijn geconfigureerd. De y-as toont de verschillende dagen van de simulatie. Voor elke dag en voor elke locatie geeft de figuur aan of een drempelwaarde is overschreden \(rode kleur\) of niet \(groene kleur\).

### 3. Simulatie details

Deze sectie geeft in meer detail de voortgang en de status van de verschillende simulatie en proces delen van de modellentrein. Zie sectie [algemene simulatie concepten](/simulations/sdf.md) voor meer uitleg over deze concepten.

De lijst bevat alle delen van de modellentrein. Voor elk deel zijn enkele algemene metadata gegeven, alsook de status. Klik op een bepaalde deel om meer informatie te verkrijgen over dat deel van de modellentrein. De resulterende tabel lijst alle verschillende stappen van een simulatie of proces onderdelen op. Voor elk onderdeel is de startdatum en de status gegeven.

### 4. Log file

De simulatie details geven een eerste inschatting van de status van een simulatie of geven aan waar het fout is gelopen. Echter, indien er meer informatie is gewenst, kan de operator de log file van elke simulatie bekijken door te klikken op de knop `View Log`. Een nieuw venster zal zich openen met de inhoud van een gedetailleerde log file. Tijdens de simulatie zal het systeem immers op meerdere momenten informatie wegschrijven in de log file zodat operators en ontwikkelaars de progressie kunnen volgen  en potentiële problemen kunnen identificeren. Het is met andere woorden steeds een goed idee te starten met de log file wanneer men een foutmelding tijdens de simulatie onderzoekt en tracht te verhelpen.

