# Simulation overview

De "simulation overview" tab geeft een lijst van alle simulaties in het systeem. Voor elke simulatie wordt de volgende informatie getoond:




| Kolom naam | Beschrijving|
| :--- | :--- |
| Name | De naam en unieke id van de simulatie |
| Modeltrain | De modellentrein gebruikt in de run|
| Description | De beschrijving van de run|
| Run Origin | t0|
| Start Date | Startdatum van de run |
| End Date | Einddatum van de run|
| Thresholds | Geeft aan of de run resulteert in een drempeloverschrijding. Mogelijke waarden zijn: <br>![](/assets/zeergoed.PNG): Geen overschrijding <br>![](/assets/aanvaardbaar.PNG): Overschrijding eerste threshold <br>![](/assets/slecht.PNG): overschrijding limietwaarde
| Status | Geeft de status van de run aan. Mogelijke waarden zijn: aanvaard, succes, aan het runnen en gefaald|
| Progress | Geeft de voortgang van een run aan. De voortgang wordt getoond door middel van 2 voortgangsbalken. <br> De eerste voortgangsbalk geeft de progressie van de modellentrein zelf aan. De balk geeft met andere woorden aan welk simulatie of proces deel aan het runnen is in de modellentrein. Het geeft een indicatie hoeveel onderdelen nog volgen. <br> De tweede voortgangsbalk geeft een indicatie van de progressie van het simulatie of proces deel dat op dat moment aan het runnen is, bv. de lopende Telemac simulatie. |
| Options | Deze kolom bevat alle acties dat kunnen worden genomen voor deze run. Klik op één van de volgende iconen om actie te ondernemen: : <br>![](/assets/Simulation_detail.png): Bekijk de  [simulation details](/simulation-details-page.md) pagina <br>![](/assets/Automated_report.png): Open het geautomatiseerde rapport (niet actief) <br>![](/assets/ViewResults.png): Ga naar de _MapViewer_ om de resultaten van een simulatie te bekijken. <br>![](/assets/approve.PNG): Aanvaard en publiceer de resultaten <br> ![](/assets/unapprove.PNG): Aanvaard de resultaten niet en publiceer ze niet  <br> ![](/assets/delete_kill.PNG): Verwijder en stop de simulatie 



