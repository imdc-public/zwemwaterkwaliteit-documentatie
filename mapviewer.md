# MapViewer

De "MapViewer" is de plek waar de operator alle data kan inspecteren op interactieve kaarten en grafieken. In één beeld kan de operator de volgende zaken laden:

* Tijdreeksen van metingen op bepaalde locaties
* Simulatieresultaten op punt locaties

Alle tijdreeksen worden geplot in interactieve grafieken waar de data van verschillende bronnen kan worden vergeleken \(bv. metingen en simulaties\). Daarnaast kunnen alle tijdsafhankelijke kaartlagen \(zoals simulatieresultaten\) in een animatie worden getoond \(niet geactiveerd in deze setup\).

# Algemene concepten

Er zijn verschillende manieren om tot de "MapViewer" te komen. Het getoonde beeld van de "MapViewer" is afhankelijk van welk toegangspunt is gebruikt:

* Door het klikken op de "\_MapViewer" \_link in de menubalk. Dit toont in eerste instantie slechts de basiskaart.
* Door het klikken op de _View Results_ knop in de [_simulation overview_](/simulation-overview.md) tabel. Dit resulteert in een "MapViewer" waarin de aangeklikte simulatie is geladen.
* Door het klikken op de "\_View On Map" \_knop naast de [observaties](/dashboard.md). Dit resulteert in een "MapViewer" waarin de toegepaste observatiedata van de voorbije maand zijn geladen.
* Door het klikken op de permalink die u verkregen hebt via een andere gebruiker. Dit resulteert in hetzelfde beeld als toen de link werd aangemaakt. .

De "_MapViewer" \_bestaat uit verschillende modules. De functionele modules komen overeen met de verschillende modules  die aan bod kwamen in de voorgaande delen van deze handleiding. Deze zijn gegroepeerd aan de linkerzijde van het beeld. De overige, algemene modules bevatten informatie en functionaliteiten die gedeeld is door de modules. Deze zijn gegroepeerd aan de rechterzijde van de_ "MapViewer"\_.

## Grafieken

Indien u een observatiepunt of een outputpunt selecteert, verschijnt er een grafiek. Dit betekent dat de kaart zal krimpen en plaats maakt voor een grafiek onderaan het scherm. Alle grafieken in het systeem zijn interactief wat betekent dat u kan inzoomen of schuiven in het beeld, en specifieke waarden kan zien. De volgende secties zullen enkele algemene concepten van een grafiek verder verduidelijken, alsook enkele aspecten specifiek voor observatiedata en simulatiedata.

### View, analyze en export

De waarden van de variabelen getoond in de grafiek kunnen worden gelezen door met muis over de tijdreeks te zweven. Indien er op één locatie meerdere variabelen beschikbaar zijn, zal de eerste variabele getoond worden op de primaire y-as \(links\) en de overige variabelen op de secondaire y-as \(rechts\). De grafiek kan worden geëxporteerd door te klikken op het    ![](/assets/ExportGraph.png) icoon. De mogelijke formaten zijn:

* PNG
* JPEG
* PDF
* SVG

In de linkerbovenhoek van de grafiek bevinden zich de zoom opties:

* ![](/assets/HorZoom.png): Selecteer de horizontale zoom optie \(default\)
* ![](/assets/VerZoom.png): Selecteer de verticale zoom optie
* ![](/assets/PanZoom.png): Selecteer de pan optie

Zoomen gebeurt door de muisaanwijzer te slepen van links naar rechts, of van beneden naar boven, afhankelijk van de zoom optie.  Klik op de knop`Reset zoom`om het beeld te resetten naar het initiële beeld. De pan optie kan worden gebruikt door de linkermuisknop in te houden en de grafiek rond te slepen. Wanneer de pan optie niet is geselecteerd, is dit ook mogelijk door eerst op de shift knop te drukken alvorens te klikken op de grafiek.

![](/assets/GraphZoomed.png)

### Combineer twee sites

De data van de twee observatiepunten of twee outputpunten kunnen worden gevisualiseerd op dezelfde grafiek. Door 1 punt te selecteren verschijnt de tijdreeks in een popup bovenaan de popup is een + te zien. Door hierop te klikken en daarna een nieuw punt te selecteren worden de tijdreeksen van dit nieuwe punt toegevoegd.  De zichtbaarheid van elke reeks in de grafiek gelinkt aan een bepaald punt kan worden geselecteerd in groep door te klikken op de reeks in het paneel onder van de grafiek. Belangrijk is hierbij dat reeksen met dezelfde eenheid op dezelfde as getoond worden. Indien een reeks met zeer hoge waarden toegevoegd wordt aan een reeks met lage waarden zal deze laatste daarom moeilijk te zien zijn. Dit is ondermeer het geval bij concentraties in de overstorten en in de simulatie punten langs de kust. Het kan de interpretatie vereenvoudigen door de concentraties van het overstort uit te klikken maar de debieten aan te laten staan. 


### Combineer simulaties met metingen

Om simulatieresultaten te combineren met metingen is het noodzakelijk om de zichtbaarheid van de observatiereeks aan te klikken en de simulatie output punten in te laden op de kaart. Eens beide zichtbaar zijn op de kaart kan een locatiepunt worden aangeklikt met zowel observatiedata als simulatiedata. De metingen en de simulatie output krijgen een verschillend lijntype. Beide reeksen kunnen zichtbaar gemaakt worden of verwijderd worden op een gelijkaardige manier zoals hierboven verduidelijkt.

## Share URL

De `Share URL` functie bevindt zich aan de rechterkant van de _MapViewer _ en laat de gebruiker toe om een kaart in een specifieke toestand te delen met andere gebruikers van het systeem \(operators met login\). De operator verkrijgt een unieke link dat kan worden gedeeld met andere gebruikers. Wanneer de andere persoon klikt op de link ziet deze meteen de simulatie, kaart lagen, grafieken en tijdstap de operator had wanneer de url werd aangemaakt. De functie laat met andere woorden toe snel interssante informatie te delen met andere gebruikers.

![](/assets/share2.png)

